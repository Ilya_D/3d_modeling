//motor_pins
const int MOTOR_PWM = 6; //управление скоростью мотора
const int MOTOR_R = 5;  // направление
const int MOTOR_L = 4;  // направление
/*
  Подключение драйвера мотора к Arduino по 3 проводам:
  Выводы «L_EN» и «R_EN» драйвера соединяются друг с другом и подключаются к любому выводу Arduino поддерживающему ШИМ.
  Вывод «L_PWM» подключается к MOTOR_R.
  Вывод «R_PWM» подключается к MOTOR_L.
  Управление драйвером мотора по 3 проводам:
  Движение вперёд с регулировкой скорости: «L_PWM» = 0, «R_PWM» = 1, «EN» = ШИМ (чем выше ШИМ тем выше скорость).
  Движение назад с регулировкой скорости: «L_PWM» = 1, «R_PWM» = 0, «EN» = ШИМ (чем выше ШИМ тем выше скорость).
  Свободное вращение: «L_PWM» и «R_PWM» не имеют значения, «EN» = 0 (мотор электрически отключён).
  Торможение: «L_PWM» = «R_PWM» = 0 или 1, «EN» = ШИМ (чем выше ШИМ тем сильнее торможение).
*/
//buttons_pin
const int BUTTON_REGIMS = A3;            // пин выбора режима работы двери
//concevik_pins
const int CONCEVIK_CLOSE = A4;        // концевик закрытой двери
const int CONCEVIK_OPEN = A5;        // концевик открытой двери
const int CONCEVIK_3 = A6;        // концевик промежуточный
const int RELAY_SECURITY = A7;  // реле для безопасников
//datchik_pins
const int KEY_READER = 2;         // key_reader
const int MOVE_IN = 7;            // датчик движения внутри каюты
const int MOVE_OUT = 3;           // датчик движения снаружи каюты
//relay_pin
const int RELAY = 12;             // реле для сигнала на пульт капитана
const int RELAY_LIGHT = 11;             // реле для света
// pot_pins
const int POT_MOTOR_SPEED = A0;   // потенциометр регулировки скорости движения двери
const int POT_OPEN_DELAY = A1;    // потенциометр регулировки времени открытия двери
const int POT_RESERVE = A2;       // запасной потенциометр
// light_button
const int LIGHT_ON = 9;
const int LIGHT_AUTO = 8;
// VOIDS
void motor_tuda () {
  digitalWrite(MOTOR_R, LOW );    // Устанавливаем логический 0 на входе драйвера L_PWM, значит на выходе драйвера M- будет установлен потенциал S-
  digitalWrite(MOTOR_L, HIGH);    // Устанавливаем логическую 1 на входе драйвера R_PWM, значит на выходе драйвера M+ будет установлен потенциал S+
  analogWrite (MOTOR_PWM, 150);
  //analogWrite (MOTOR_PWM, (analogRead (POT_MOTOR_SPEED) / 8)); // Устанавливаем скорость. значение задается потенциометром
}
void motor_suda () {
  digitalWrite(MOTOR_R, HIGH );   // Устанавливаем логический 1 на входе драйвера L_PWM, значит на выходе драйвера M- будет установлен потенциал S-
  digitalWrite(MOTOR_L, LOW);     // Устанавливаем логическую 0 на входе драйвера R_PWM, значит на выходе драйвера M+ будет установлен потенциал S+
  analogWrite (MOTOR_PWM, 150);
  //analogWrite (MOTOR_PWM, (analogRead (POT_MOTOR_SPEED) / 8)); // Устанавливаем скорость. значение задается потенциометром
}
void motor_tuda_slowly () {
  digitalWrite(MOTOR_R, LOW );   // Устанавливаем логический 1 на входе драйвера L_PWM, значит на выходе драйвера M- будет установлен потенциал S-
  digitalWrite(MOTOR_L, HIGH);     // Устанавливаем логическую 0 на входе драйвера R_PWM, значит на выходе драйвера M+ будет установлен потенциал S+
  analogWrite (MOTOR_PWM, 100);
  //analogWrite (MOTOR_PWM, (analogRead (POT_MOTOR_SPEED) / 16)); // Устанавливаем скорость. значение задается потенциометром
}
void motor_stop () {
  digitalWrite(MOTOR_R, LOW );   // Устанавливаем логический 1 на входе драйвера L_PWM, значит на выходе драйвера M- будет установлен потенциал S-
  digitalWrite(MOTOR_L, LOW );     // Устанавливаем логическую 0 на входе драйвера R_PWM, значит на выходе драйвера M+ будет установлен потенциал S+
  analogWrite (MOTOR_PWM, 0); // Устанавливаем скорость. значение задается потенциометром
  delay (100);
}
void door_open () {
  digitalWrite (RELAY, HIGH);
  if (digitalRead (LIGHT_AUTO)) {
    digitalWrite (RELAY_LIGHT, HIGH);
  }
  while (analogRead (CONCEVIK_OPEN) < 1000) { // открываем дверь до концевика
    motor_suda ();
    delay(10);
    Serial.println ("motor_suda");
  }
  motor_stop ();  // мотор стоп
  Serial.println ("motor_stop");
  Serial.println ("func door_OPEN complite");
  delay (5000); // время открытия двери для прохода людей
}
void door_close () {
  while (analogRead (CONCEVIK_CLOSE) < 1000) {  // закрываем дверь до концевика
    motor_tuda ();
    Serial.println ("motor_tuda");
    delay(10);
    if (digitalRead (MOVE_IN) || digitalRead (MOVE_OUT)) {
      if (digitalRead (MOVE_IN)) {
        Serial.print ("внутри ");
      }
      if (digitalRead (MOVE_OUT)) {
        Serial.print ("снаружи ");
      }
      Serial.println ("кто-то пошел");
      motor_stop ();
      delay(10);
      Serial.println ("motor_stop");
      delay (500);
      door_open ();
    }
    if (analogRead (CONCEVIK_3) > 1000) {
      while (analogRead (CONCEVIK_CLOSE) < 1000) {
        motor_tuda_slowly ();
        Serial.println ("motor_tuda_slowly");
        delay(100);
      }
    }
  }
  motor_stop ();
  if (digitalRead (LIGHT_AUTO)) {
    digitalWrite (RELAY_LIGHT, HIGH);
  }
  digitalWrite (RELAY, LOW);
  Serial.println ("func door_CLOSE complite");
}

void setup () {
  Serial.begin (9600);
  //motor_pins
  pinMode(MOTOR_PWM, OUTPUT);
  pinMode(MOTOR_R, OUTPUT);
  pinMode(MOTOR_L, OUTPUT);
  //button_pin
  pinMode (BUTTON_REGIMS, INPUT);
  pinMode (LIGHT_ON, INPUT);
  pinMode (LIGHT_AUTO, INPUT);
  //concevik_pins
  pinMode (CONCEVIK_CLOSE, INPUT);
  pinMode (CONCEVIK_OPEN, INPUT);
  pinMode (CONCEVIK_3, INPUT);
  pinMode (RELAY_SECURITY, OUTPUT);
  //datchik_pins
  pinMode (KEY_READER, INPUT);
  pinMode (MOVE_IN, INPUT);
  pinMode (MOVE_OUT, INPUT);
  //relay_pin
  pinMode (RELAY, OUTPUT);
  pinMode (RELAY_LIGHT, OUTPUT);
  // pot_pins
  pinMode (POT_MOTOR_SPEED, INPUT);
  pinMode (POT_OPEN_DELAY, INPUT);
  pinMode (POT_RESERVE, INPUT);
  // if (!digitalRead (CONCEVIK_CLOSE)) {
  // door_close ();
  // }
  delay (2000);
}
void loop () {
  //motor_tuda (); // закрыть

  int door_button_state = analogRead (BUTTON_REGIMS); // чтение кнопки открытия двери изнутри
  int light_on_state = digitalRead (LIGHT_ON);
  int light_auto_state = digitalRead (LIGHT_AUTO);
  //delay (100);
  Serial.print ("состояние кнопки выбора режима = "); Serial.println (door_button_state);
  if (light_on_state == 1 ) {
    digitalWrite (RELAY_LIGHT, HIGH);
  delay (100);
  }
  else if (light_on_state == 0 ) {
    digitalWrite (RELAY_LIGHT, LOW);
    delay (100);
  }
  delay (100);
  if (door_button_state > 1000) { // выбран режим "по ключу"
    if (digitalRead (KEY_READER)) { // ключ или кнопка для открытия двери
      door_open ();
      door_close ();
    }
  }
  else if (door_button_state > 900 && door_button_state < 1000) { // выбран режим "автоматический"
    if (digitalRead (MOVE_IN) || digitalRead (MOVE_OUT)) {
      Serial.println ("кто-то пошел");
      door_open ();
      door_close ();
    }
  }
  else if (door_button_state < 900) { // выбран режим "дверь закрыта"
    Serial.println ("дверь заблокированна");
    motor_stop ();
  }
  //блок отладки
  /*int move_in_state = digitalRead (MOVE_IN);
      int move_out_state = digitalRead (MOVE_OUT);
    //  int button_open_door_state = analogRead (BUTTON_OPEN_DOOR);
      int pot_motor_speed_state = analogRead (POT_MOTOR_SPEED);
      int pot_open_delay_state = analogRead (POT_OPEN_DELAY);
      int pot_reserve_state = analogRead (POT_RESERVE);
      int key_reader_state = digitalRead (KEY_READER);
      int consevik_open_state = analogRead (CONCEVIK_OPEN);
      int consevik_close_state = analogRead (CONCEVIK_CLOSE);
      int consevik_3_state = analogRead (CONCEVIK_3);
      Serial.print ("состояние кнопки выбора режима = ");
      Serial.println (door_button_state);
    //  Serial.print ("состояние кнопки открытия двери = ");
    //  Serial.println (button_open_door_state);
      Serial.print ("состояние KEY_READER  = ");
      Serial.println (key_reader_state);
      Serial.print ("состояние датчика движения изнутри = ");
      Serial.println (move_in_state);
      Serial.print ("состояние датчика движения снаружи = ");
      Serial.println (move_out_state);
      /* Serial.print ("состояние потенциометра скорости двери = ");
      Serial.println (pot_motor_speed_state);
      Serial.print ("состояние потенциометра задержки открытия двери = ");
      Serial.println (pot_open_delay_state);
      Serial.print ("состояние резервного потенциометра  = ");
      Serial.println (pot_reserve_state);  /
      Serial.print ("состояние концевик закрыт = ");
      Serial.println (consevik_close_state);
      Serial.print ("состояние концевик открыт = ");
      Serial.println (consevik_open_state);
      Serial.print ("состояние концевик 3 = ");
      Serial.println (consevik_3_state);
      Serial.println ("====================== ");
      delay (1000); */
}
