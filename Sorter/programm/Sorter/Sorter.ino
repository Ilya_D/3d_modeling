#include <LCD_1602_RUS.h>

LCD_1602_RUS lcd(0x27, 20, 4);
// пины кнопок
const int BUT_STOP = 12;
const int BUT_START = 11;
const int BUT_DOWN = 10;
const int BUT_UP = 9;
const int BUT_PLUS = 7;
const int BUT_MINUS = 8;
const int CONC = 4;        // пин концевика
const int MOTOR_1 = 6;     // мотор раздатчика
const int MOTOR_2 = 5;     // мотор приемника
const int ENCODER = A6;    // пин энкодера
int count_metiz = 10;       // переменная для количества считаемых гаек
int count_ready_metiz = 0; // переменная посчитаных гаек
int count_ready_cap = 0;   // переменная посчитаных стаканов
int SPEED = 70;            // переменая для скорости моторов
int menu = 0;              // пункты меню
// создание стрингов для надписей (без стрингов русский язык почему-то очень хуево работает)
String str_setting = "Настройки";
String str_speed = " Скорость = ";
String str_koll = " Колличество = ";
String str_work = "Pаботаю";
String str_Sorter = "Сортировщик";
String str_metiz  = "метизов";
String str_version = "версия 0.1";

void searc_conc () {
  if (digitalRead(CONC)) {
    analogWrite (MOTOR_1, 80);
    while (digitalRead(CONC)) { // докручиваем мотор до след. срабатывания концевика

      //analogWrite (MOTOR_1, 100);
      // delay(10);
      // analogWrite (MOTOR_1, 0);
      delay(15);
    }
  }
}

void setup()
{
  Serial.begin (9600);
  pinMode (BUT_STOP, INPUT_PULLUP);
  pinMode (BUT_START, INPUT_PULLUP);
  pinMode (BUT_DOWN, INPUT_PULLUP);
  pinMode (BUT_UP, INPUT_PULLUP);
  pinMode (BUT_PLUS, INPUT_PULLUP);
  pinMode (BUT_MINUS, INPUT_PULLUP);
  pinMode (CONC, INPUT_PULLUP);
  pinMode (MOTOR_1, OUTPUT);
  pinMode (MOTOR_2, OUTPUT);
  pinMode (ENCODER, INPUT);
  searc_conc ();
  analogWrite (MOTOR_1, 0);
  analogWrite (MOTOR_2, 0);

  lcd.init();                      // initialize the lcd
  // Print a message to the LCD.
  lcd.backlight();
  lcd.setCursor(0, 1);
  lcd.print(str_Sorter);
  lcd.setCursor(12, 1);
  lcd.print(str_metiz);
  lcd.setCursor(10, 3);
  lcd.print(str_version);
  delay (2000);
  lcd.clear();
}

void loop()
{
  while (digitalRead (BUT_START)) {
    // Serial.print ("энкодер = ");
    // Serial.println (analogRead (ENCODER));
    // Serial.print ("концевик = ");
    // Serial.println (digitalRead (CONC));
    /* Serial.print ("кнопка + = ");
      Serial.println (digitalRead (BUT_PLUS));
      Serial.print ("кнопка - = ");
      Serial.println (digitalRead (BUT_MINUS));
      Serial.print ("кнопка вверх = ");
      Serial.println (digitalRead (BUT_UP));
      Serial.print ("кнопка вниз = ");
      Serial.println (digitalRead (BUT_DOWN));
      Serial.print ("кнопка Стврт = ");
      Serial.println (digitalRead (BUT_START));
      delay(200); */
    analogWrite (MOTOR_1, 0);
    analogWrite (MOTOR_2, 0);
    Serial.println ("Настраиваюсь");
    if (menu == 0) {
      lcd.setCursor(5, 0);
      lcd.print(str_setting);
      lcd.setCursor(0, 1);
      lcd.print(char (126));
      lcd.setCursor(1, 1);
      lcd.print(str_koll);
      lcd.print(count_metiz);
      // дописываем "пустоту" дабы убрать последние символы после использования десяток и сотен
      lcd.print("   ");
      lcd.setCursor(0, 2);
      lcd.print(" ");
      lcd.setCursor(1, 2);
      lcd.print(str_speed);
      lcd.print(SPEED);
      // дописываем "пустоту" дабы убрать последние символы после использования десяток и сотен
      lcd.print("   ");
      if (!digitalRead (BUT_PLUS) && count_metiz <= 99) {
        delay(100);
        count_metiz++;
      }
      else if (!digitalRead (BUT_PLUS) && count_metiz == 100) {
        delay(100);
        count_metiz = 0;
      }
      else if (!digitalRead (BUT_MINUS) && count_metiz > 0) {
        delay(100);
        count_metiz--;
      }
      else if (!digitalRead (BUT_MINUS) && count_metiz == 0) {
        delay(100);
        count_metiz = 100;
      }
      else if (!digitalRead (BUT_UP) || !digitalRead (BUT_DOWN)) {
        delay (100);
        menu = 1;
      }
    }
    else if (menu == 1) {
      // создание стрингов для надписей (без стрингов русский язык почему-то очень хуево работает)
      lcd.setCursor(5, 0);
      lcd.print(str_setting);
      lcd.setCursor(0, 1);
      lcd.print(" ");
      lcd.setCursor(1, 1);
      lcd.print(str_koll);
      lcd.print(count_metiz);
      // дописываем "пустоту" дабы убрать последние символы после использования десяток и сотен
      lcd.print("   ");
      lcd.setCursor(0, 2);
      lcd.print(char (126));
      lcd.setCursor(1, 2);
      lcd.print(str_speed);
      lcd.print(SPEED);
      // дописываем "пустоту" дабы убрать последние символы после использования десяток и сотен
      lcd.print("   ");
      if (!digitalRead (BUT_PLUS) && SPEED <= 99) {
        delay(100);
        SPEED++;
      }
      else if (!digitalRead (BUT_PLUS) && SPEED == 100) {
        delay(100);
        SPEED = 0;
      }
      else if (!digitalRead (BUT_MINUS) && SPEED > 0) {
        delay(100);
        SPEED--;
      }
      else if (!digitalRead (BUT_MINUS) && SPEED == 0) {
        delay(100);
        SPEED = 100;
      }
      else if (!digitalRead (BUT_UP) || !digitalRead (BUT_DOWN)) {
        delay (100);
        menu = 0;
      }
    }
  }
  lcd.clear();
  lcd.setCursor(5, 0);
    lcd.print(str_work);
     lcd.setCursor(0, 1);
    // lcd.print(str_in);
   // lcd.print(count_ready_metiz);
  delay(500);
  while (digitalRead (BUT_START)) {
    //Serial.println ("РАботаю");
    // экран
    // delay(100);


   // String str_in;
   // str_in = " в стакане = ";
    //String str_koll;
    //str_koll = " стаканов = ";
    // String str_summ;
    // str_summ = " всего = ";
    
    //  lcd.setCursor(0, 2);
    //  lcd.print(str_koll);
    // lcd.setCursor(0, 3);
    //  lcd.print(str_summ);

    // работа
    //searc_conc ();
    analogWrite (MOTOR_2, SPEED);
   // Serial.print ("энкодер = ");
   // Serial.println (analogRead (ENCODER));
    int enco_count = analogRead(ENCODER);
   // Serial.println (enco_count);
    if (enco_count > 950) {
      while (analogRead(ENCODER) > 930) {
        delay(2);
        Serial.print ("гайка");
        Serial.println (analogRead(ENCODER));
      }
      count_ready_metiz++;
      Serial.print ("гайка++");
      Serial.println (count_ready_metiz);
      if (count_ready_metiz == count_metiz) {
        analogWrite (MOTOR_2, 0);
        delay(1000);
        analogWrite (MOTOR_1, 120);
        delay(175);// задержка что бы съехать с концевика
        analogWrite (MOTOR_1, 70);
        Serial.print ("++++++++++++");
        // delay(100);// задержка что бы съехать с концевика
        while (digitalRead(CONC)) { // докручиваем мотор до след. срабатывания концевика
          delay(1);
        }
        analogWrite (MOTOR_1, 0);
        count_ready_cap++; // добавляем стакан в посчитанные
        count_ready_metiz = 0; // обнуляем переменную посчитаных гаек
        /* if (count_ready_cap == 8) { // если все стаканы наполнены
           // здесь должен быть кусок кода который ставит на паузу работу и спрашивает "Продолжить?"
           // и выбор продолжить сыпать в стаканы или нет
           lcd.clear();
           String str_cap_full;
           str_cap_full = "стаканы наполнены";
           lcd.setCursor(5, 0);
           lcd.print(str_cap_full);
          }*/
      }
    }
  }
  lcd.clear();
  delay(500);
}
