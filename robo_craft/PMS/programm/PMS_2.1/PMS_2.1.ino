#include <GyverOLED.h>

#include <GyverBME280.h> // библиотека для BMP280

GyverOLED<SSD1306_128x64, OLED_NO_BUFFER> oled; // экран
GyverBME280 bmp; // датчик давления

void setup()
{
  Serial.begin(9600);
  bmp.begin(0x76);
  oled.init();        // инициализация
  oled.clear();       // очистка
  oled.setScale(2);   // масштаб текста (1..4)
  oled.setCursorXY(0, 16);

  //////////////////////
  oled.setScale(2);   // масштаб текста (1..4)
  oled.clear();       // очистка
  oled.setCursorXY(15, 0);
  oled.print("Походная");
  oled.setCursorXY(15, 16);
  oled.print("Метео ");
  oled.setCursorXY(15, 32);
  oled.print("Станция ");
  oled.setScale(1);   // масштаб текста (1..4)
  oled.setCursorXY(65, 55);
  oled.print("версия 2.1");
  delay (2000);

  //////////////////////
  oled.clear();       // очистка
  oled.setScale(2);   // масштаб текста (1..4)
  oled.setCursorXY(15, 17);
  oled.print("Собираю");
  oled.setCursorXY(15, 33);
  oled.print("данные");
  oled.setScale(1);   // масштаб текста (1..4)
  oled.setCursorXY(65, 55);
  oled.print("версия 2.1");
  for (int x = 0; x < 128; x++) {
    oled.rect(0, 0, x, 15, OLED_FILL);
    delay(5);
  }
  oled.clear();       // очистка
  
}

void loop()
{
  // проверка на работоспособность датчиков
 /* if (!bmp.begin(0x76)) {
    while (1) {
      oled.println(" Error!");
    }
  }*/
  oled.setScale(1);   // масштаб текста (1..4)
  oled.update();       // очистка
  //delay(2000);
  int H = bmp.readPressure() / 133.3;
  // температура
  Serial.print("Temperature: ");
  Serial.print(bmp.readTemperature());
  // давление
  Serial.print(" Pressure: ");
  Serial.print(bmp.readPressure() / 133.3);
  // влажность
  Serial.print (" Humidity = ");
  Serial.println (bmp.readHumidity());

  oled.setCursorXY(0, 16);
  oled.print("Температура ");
  oled.print(bmp.readTemperature());
  oled.print(" C");
  oled.line(0, 26, 128, 26);
  oled.setCursorXY(0, 32);
  oled.print("Влажность ");
  oled.print(bmp.readHumidity());
  oled.print(" %");
  oled.line(0, 42, 128, 42);
  oled.setCursorXY(0, 48);
  oled.print("Давление ");
  oled.print(H);
  oled.print(" мм.рт.ст");
  oled.line(0, 58, 128, 58);
  delay(1000);
}
