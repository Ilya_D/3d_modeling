#include <LiquidCrystal_I2C.h>

#include <NewPing.h>
#include <Wire.h>                             // Для работы с шиной I2C
#include "Adafruit_TCS34725.h"                // Для работы с датчиком TCS34725
LiquidCrystal_I2C lcd(0x27, 16, 2); // Устанавливаем дисплей

const int TRIG = 5; // датчик сонар
const int ECHO = 4; // датчик сонар
const int OUT_IR = 6; // ИК приемник
const int BUTTON = 7; // кнопка переключения режимов
const int SIG_1 = 9; // датчик кнопка
const int SIG_2 = 8;// датчик кнопка
const int SIG_3 = 10;// датчик кнопка
const int SIG_4 = 11;// датчик кнопка
const int LED = 12; //датчик цвета
const int INT = 13;//датчик цвета
const int SDA_ = A4;//датчик цвета
const int SCL_ = A5;//датчик цвета
const int U_1 = A0; //датчик линии
const int U_2 = A1; //датчик линии
//определение сонара
const int MAX_DISTANCE = 200;
NewPing sonar(TRIG, ECHO, MAX_DISTANCE);
// определение датчика цвета
byte gammatable[256];                         // Гамма-таблица визуально различимых глазом цветов
Adafruit_TCS34725 tcs = Adafruit_TCS34725(TCS34725_INTEGRATIONTIME_50MS, TCS34725_GAIN_60X);
// Определяем объект tcs, экземпляр класса Adafruit_TCS34725
// Устанавливаем время выборки и чувствительность сенсора
// Время выборки может быть одним из значений: TCS34725_INTEGRATIONTIME_2_4MS,
// TCS34725_INTEGRATIONTIME_24MS, TCS34725_INTEGRATIONTIME_50MS,
// TCS34725_INTEGRATIONTIME_101MS, TCS34725_INTEGRATIONTIME_154MS,
// TCS34725_INTEGRATIONTIME_700MS
// Чувствительность сенсора может быть одним из значений: TCS34725_GAIN_1X,
// TCS34725_GAIN_4X, TCS34725_GAIN_16X, TCS34725_GAIN_60X
int menu = 0;

void setup() {
  Serial.begin(9600);
  tcs.begin();   // инициализация датчика цвета
  lcd.init();
  lcd.backlight();// Включаем подсветку дисплея
  lcd.print("  sensor check");
  lcd.setCursor(10, 1);
  lcd.print("v 1.0");
  pinMode(TRIG, OUTPUT);
  pinMode(ECHO, INPUT);
  pinMode(SIG_1, INPUT);
  pinMode(SIG_2, INPUT);
  pinMode(SIG_3, INPUT);
  pinMode(SIG_4, INPUT);
  pinMode(OUT_IR, INPUT);
  pinMode(BUTTON, INPUT_PULLUP);
  pinMode(LED, INPUT);
  pinMode(INT, INPUT);
  pinMode(U_1, INPUT);
  pinMode(U_2, INPUT);
  pinMode(SDA_, INPUT);
  pinMode(SCL_, INPUT);
  delay (2000);
  lcd.clear();
}

void loop() {
  if (!digitalRead (BUTTON)) {
    while (!digitalRead (BUTTON)) {
      delay (100);
    }
    lcd.clear();
    menu++;
    if (menu == 5) {
      menu = 0;
    }
  }
  Serial.print ("menu = ");
  Serial.println (menu);
  if (menu == 0 ) {
    while (digitalRead (BUTTON)) {
      // ПРОВЕРКА СОНАРА
      delay(50);
      unsigned int distance = sonar.ping_cm();
      lcd.setCursor(0, 0);
      lcd.print (" Sonar sensor");
      lcd.setCursor(0, 1);
      lcd.print (" distance = ");
      //lcd.setCursor(12, 1);
      lcd.print (distance);
      lcd.print ("    ");
      Serial.print(distance);
      Serial.println("см");
    }
  }
  if (menu == 1 ) {
    while (digitalRead (BUTTON)) {
      // ПРОВЕРКА КНОПКИ
      lcd.setCursor(0, 0);
      lcd.print (" switch sensor");
      lcd.setCursor(0, 1);
      lcd.print (" switch = ");
      //lcd.setCursor(12, 1);
      lcd.print (digitalRead (SIG_4));
      Serial.print ("SIG_4 = ");
      Serial.println (digitalRead (SIG_4));

    }
  }
  if (menu == 2 ) {
    while (digitalRead (BUTTON)) {
      // ПРОВЕРКА ДАТЧИК ЦВЕТА
      uint16_t clear, red, green, blue;             // Переменные для красной, зеленой и синей составляющих цвета
      tcs.setInterrupt(false);                      // Включаем светодиод
      delay(60);                                    // Ждем как минимум 50 миллисекунд для выборки значений
      tcs.getRawData(&red, &green, &blue, &clear);  // Читаем данные
      tcs.setInterrupt(true);                       // Выключаем светодиод
      // Выводим в монитор последовательного порта полученные значения
      lcd.setCursor(0, 0);
      lcd.print (" color sensor   ");
      //lcd.setCursor(12, 1);
      lcd.print (digitalRead (SIG_4));
      if (red < 4000 && blue < 4000 && green < 4000) {
        Serial.println("нет цвета");
        lcd.setCursor(0, 1);
        lcd.print (" no color ");
      }
      else if (red > green && red > blue) {
        Serial.println("red");
        lcd.setCursor(0, 1);
        lcd.print (" Red                ");
      }
      else if (green > red && green > blue) {
        Serial.println("green");
        lcd.setCursor(0, 1);
        lcd.print (" Green                ");
      }
      else if (blue > red && blue > green) {
        Serial.println("blue");
        lcd.setCursor(0, 1);
        lcd.print (" Blue                ");
      }
    }
  }
  if (menu == 3 ) {
    while (digitalRead (BUTTON)) {
      // ПРОВЕРКА ИК-приемник
      lcd.setCursor(0, 0);
      lcd.print (" IR sensor      ");
      //lcd.setCursor(12, 1);
      if (!digitalRead (OUT_IR)) {
        Serial.println ("IR = есть сиганал");
        lcd.setCursor(0, 1);
        lcd.print ("this is a signal           ");
        delay (500);
      }
      else  lcd.setCursor(0, 1); lcd.print ("                  ");
    }
  } if (menu == 4 ) {
    while (digitalRead (BUTTON)) {
      // ПРОВЕРКА ДАТЧИК ЛИНИИ
      lcd.setCursor(0, 0);
      lcd.print (" line sensor");
      lcd.setCursor(0, 1);
      lcd.print ("U1 = ");
      lcd.print (digitalRead (U_1));
      Serial.print ("U_1 = ");
      Serial.print (digitalRead (U_1));
      lcd.print (" U2 = ");
      lcd.print (digitalRead (U_2));
      Serial.print ("    U_2 = ");
      Serial.println (digitalRead (U_2));
    }
  }
}
