#include <Wire.h>
#include <LiquidCrystal_I2C.h>
LiquidCrystal_I2C lcd(0x27, 16, 2); // Устанавливаем дисплей

const int BUTTON_START = 2;
const int BUTTON_PLUS = 3;
const int BUTTON_MINUS = 4;
const int MOTOR_PIN = 5;
const int TRAN = A0;

int LENGHT = 145; // переменная для длинны веревки
int counter = 0; // переменная для счета энкодера (1 counter == 1 LENGHT )
int flag_tran = 0;

void setup()
{
  Serial.begin (9600);
  lcd.init();
  lcd.backlight();// Включаем подсветку дисплея
  lcd.print("Hello");
  lcd.setCursor(8, 1);
  lcd.print("People");
  pinMode (BUTTON_START, INPUT_PULLUP);
  pinMode (BUTTON_PLUS, INPUT_PULLUP);
  pinMode (BUTTON_MINUS, INPUT_PULLUP);
  pinMode (TRAN, INPUT);
  pinMode (MOTOR_PIN, OUTPUT);
  analogWrite (MOTOR_PIN, 0);
  delay(2000);
  lcd.clear ();
}
void loop()
{
  /* блок отладки
    Serial.print ("состояние кнопки START = ");
    Serial.println (digitalRead (BUTTON_START));
    Serial.print ("состояние BUTTON_PLUS = ");
    Serial.println (digitalRead (BUTTON_PLUS));
    Serial.print ("состояние BUTTON_MINUS = ");
    Serial.println (digitalRead (BUTTON_MINUS));
    Serial.print ("состояние TRAN = ");
    Serial.println (analogRead (TRAN));
    Serial.println ("================== ");
    delay (400);
  */

  lcd.setCursor(0, 0);
  lcd.print("installed = ");
  lcd.setCursor(12, 0);
  lcd.print(LENGHT);
  delay(100);

  if (!digitalRead (BUTTON_PLUS)) {
    delay(50); // задержка для защиты от дребезга
    if (!digitalRead (BUTTON_PLUS)) {
      LENGHT++;
      lcd.clear ();
      if (LENGHT > 999) {
        LENGHT = 0;
      }
    }
  }
  if (!digitalRead (BUTTON_MINUS)) {
    delay(50); // задержка для защиты от дребезга
    if (!digitalRead (BUTTON_MINUS)) {
      LENGHT--;
      lcd.clear ();
      if (LENGHT < 0) {
        LENGHT = 999;
      }
    }
  }

  if (!digitalRead (BUTTON_START)) {
    delay(50);
    if (!digitalRead (BUTTON_START)) {
      analogWrite (MOTOR_PIN, 255); // включаем мотор
      while (counter < (LENGHT - (LENGHT / 9))) {
        int tran_state = analogRead(TRAN);
        // Serial.print ("состояние TRAN = ");
        // Serial.println (analogRead (TRAN));
        //delay (2);
        if (tran_state > 50 && flag_tran == 0) { // щелкнул энкодер
          counter++;
          flag_tran = 1;
          lcd.setCursor(0, 1);
          lcd.print("counter = ");
          lcd.setCursor(12, 1);
          lcd.print(counter);
          Serial.print ("счет =  ");
          Serial.println (counter);
        }
        else if (tran_state < 50 && flag_tran == 1) {
          flag_tran = 0;
        }
      }
      lcd.clear (); //чистим дисплей
      lcd.setCursor(0, 1);
      lcd.print("FINISH ");
      analogWrite (MOTOR_PIN, 0); // выключаем мотор
      counter = 0; // сбрасываем переменную
      delay (2000);
      lcd.clear (); //чистим дисплей
    }
  }
}
