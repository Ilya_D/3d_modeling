#include <SoftwareSerial.h>
SoftwareSerial BT(12, 13); // RX, TX
int BT_val; // значение приходящие с блютузмодуля

const int DIR = 2;
const int STEP = 5;
const int ENABLE = 8;
const int STARTING_SPEED = 15000;  // начальная скорость ускорения (задержка между импульсами на мотор)
const int ACCELERATION = 100;      // ускорение (уменьшение задержки между импульсами)
const int SPEED_MOTOR = 700;       // скорость вращения мотора (задержка между импульсами на мотор)
const long int AMOUNT_OF_FEED = 50000;   // количество корма (сколько вращается мотор)


void acceleration_suda ()
{
  digitalWrite(ENABLE, LOW);
  digitalWrite(DIR, HIGH); // Устанавливаем направление
  for (int i = STARTING_SPEED; i > SPEED_MOTOR; i = i - ACCELERATION)
  {
    BT_val = BT.read();
    if (BT_val == 48 )
    {
      Serial.println (BT_val);
      Serial.println ("остановка");
      digitalWrite(ENABLE, HIGH);
      break;
    }
    digitalWrite(STEP, LOW);
    digitalWrite(STEP, HIGH); // В этих двух строках LOW и HIGH дается команда шаговому двигателю двигатьс
    delayMicroseconds(i); // Эта задержка соответствует максимальной скорости данного конкретного шагового двигателя
  }
}

void motor_suda ()
{
  digitalWrite(ENABLE, LOW);
  digitalWrite(DIR, HIGH); // Устанавливаем направление
  for (int i = 0; i < AMOUNT_OF_FEED; i++)
  {
    BT_val = BT.read();
    if (BT_val == 48 )
    {
      Serial.println (BT_val);
      Serial.println ("остановка");
      digitalWrite(ENABLE, HIGH);
      break;
    }
    digitalWrite(STEP, LOW);
    digitalWrite(STEP, HIGH); // В этих двух строках LOW и HIGH дается команда шаговому двигателю двигатьс
    delayMicroseconds(SPEED_MOTOR); // Эта задержка соответствует максимальной скорости данного конкретного шагового двигателя
  }
}

void hand_suda ()
{
  acceleration_suda ();
  motor_suda ();
}

void setup()
{
  pinMode (DIR, OUTPUT);
  pinMode (STEP, OUTPUT);
  pinMode (ENABLE, OUTPUT);
  BT.begin(115200);
  Serial.begin (9600);
}

void loop()
{
  BT_val = BT.read();
  Serial.println (BT_val);
  if (BT_val == 49 )
  {
    Serial.println (BT_val);
    delay(1000);
    hand_suda ();
  }
}
